## What's different in this fork?

- Support for custom images, just put them in `src/img/backgrounds`, or better, make it a symlink. (folder shouldn't have anything other than images)
- Added fadein and fadeout animations before displaying greeter and after successful authentication.
- Removed some clutter, such as Avatar and Multi-user logins ():D
- Background and `login-window` `background` changes with every boot.
- Removed click handlers on Arch logo, as it was unnecessary.
- I hate rounded corners, so I made some improvements :D
- Removed Suspend and Hibernate as I don't use them, but you can re-add them in `CommandPanel.jsx`, just compare it with upstream, you'll notice the difference.
- More little changes, usually style.

**Try it out [here, at the live demo](https://omeryagmurlu.github.io/Sirma/).**

![](../screenshots/screenshot.png)

![](../screenshots/screenshot-2.png)

![](../screenshots/screenshot-3.png)

![](../screenshots/full.gif)

![](../screenshots/half.gif)

A fork of lovely [Aether theme](https://github.com/NoiSek/Aether).
