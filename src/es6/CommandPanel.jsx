import Inferno from 'src/dist/js/inferno.min';
import Component from 'src/dist/js/inferno-component.min';

import { WallpaperSwitcher } from "./WallpaperSwitcher";
import { Clock } from './Clock';

import { endGreeting } from './utils';

export class CommandPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "expandedCommands": false
    };
  }

  componentWillMount() {
    // Set background wallpaper
    let wallpaperImage = this.props.backgrounds[Math.floor(Math.random() * this.props.backgrounds.length)];
    let wallpaperForeground = document.querySelectorAll('.wallpaper-foreground')[0];

    wallpaperForeground.style.background = `url('src/img/backgrounds/${wallpaperImage}')`;
    wallpaperForeground.style.backgroundSize = "cover";
  }

  handleCommand(command, disabled, event) {
    if (disabled !== false) {
      window.notifications.generate(`${command} is disabled on this system.`, "error");
      return false;
    }

    if (command === "power off") {
      window.notifications.generate("Powering off.");
      endGreeting(() => window.lightdm.shutdown());
    } else if (command === "hibernate") {
      window.notifications.generate("Hibernating.");
      endGreeting(() => window.lightdm.hibernate());
    } else if (command === "restart") {
      window.notifications.generate("Restarting.");
      endGreeting(() => window.lightdm.restart());
    } else if (command === "sleep") {
      window.notifications.generate("Suspending.");
      endGreeting(() => window.lightdm.suspend());
    }
  }

  generateCommands() {
    let commands = { // hibernate and suspend were here!
      "Power Off": window.lightdm.can_shutdown,
      "Restart": window.lightdm.can_restart
    };

    // Filter out commands we can't execute.
    let enabledCommands = Object.keys(commands)
    .map((key) => commands[key] ? key : false)
    .filter((command) => command !== false);

    let rows = enabledCommands.map((command) => {
      let disabled = command.toLowerCase().split('.')[1] || false;
      command = command.toLowerCase().split('.')[0];

      let classes = ['command', command, disabled].filter((e) => e);

      return (
        <div className={ classes.join(' ') } onClick={ this.handleCommand.bind(this, command, disabled) }>
          <div class="icon-wrapper">
            <div class="icon"></div>
          </div>
          <div class="text">{ command }</div>
        </div>
      );
    });

    let classes = ['commands-wrapper'];

    return (
      <div className={ classes.join(' ') }>
        { rows }
      </div>
    );
  }

  render() {
    let hostname = window.lightdm.hostname;
    let commands = this.generateCommands();

    return (
      <div>
        <div className="distro-wrapper">
          <div className="distro-logo"></div>
        </div>
        { commands }
        <div className="bottom">
          <div className="left hostname">{ hostname }</div>
          <Clock />
        </div>
      </div>
    );
  }
}
