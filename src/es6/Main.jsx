import Inferno from 'src/dist/js/inferno.min';
import Component from 'src/dist/js/inferno-component.min';
import InfernoDOM from 'src/dist/js/inferno-dom.min';

// For some reason, this loader requires that libraries be loaded here in order for them to work elsewhere.
import { Notifications } from './Notifications';
import { CommandPanel } from './CommandPanel';
import { LoginPanel } from './LoginPanel';
import { Clock } from './Clock';

import { BACKGROUNDS } from './bgs';

export { BACKGROUNDS };

export default function Main() {
  // Add notifications to the global scope for error handling
  window.notifications = new Notifications();

  InfernoDOM.render(<CommandPanel backgrounds={ BACKGROUNDS } />, document.getElementById("command-panel"));
  InfernoDOM.render(<LoginPanel />, document.getElementById("login-panel"));
}
