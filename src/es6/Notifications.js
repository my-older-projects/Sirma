export class Notifications {
  constructor() {
    this.container = document.querySelectorAll('#messages')[0];
  }

  generate(message, type) {
    this.container.innerHTML = `<span class="${type}">${message}</span>`;
  }
}
