export function endGreeting(cb) {
  document.querySelector('html').className += ' ended';
  setTimeout(cb, 1000);
}

export function rander(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}
