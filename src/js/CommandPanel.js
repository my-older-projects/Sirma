define(['exports', 'src/dist/js/inferno.min', 'src/dist/js/inferno-component.min', './WallpaperSwitcher', './Clock', './utils'], function (exports, _inferno, _infernoComponent, _WallpaperSwitcher, _Clock, _utils) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.CommandPanel = undefined;

  var _inferno2 = _interopRequireDefault(_inferno);

  var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  var bp3 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: 'text',
    children: {
      arg: 0
    }
  });

  var bp2 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: 'icon'
  });

  var bp1 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: 'icon-wrapper',
    children: {
      arg: 0
    }
  });

  var bp0 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: {
      arg: 0
    },
    events: {
      arg: 1
    },
    children: {
      arg: 2
    }
  });

  var bp4 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: {
      arg: 0
    },
    children: {
      arg: 1
    }
  });

  var bp10 = _inferno2.default.createBlueprint({
    tag: {
      arg: 0
    }
  });

  var bp9 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: 'left hostname',
    children: {
      arg: 0
    }
  });

  var bp8 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: 'bottom',
    children: {
      arg: 0
    }
  });

  var bp7 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: 'distro-logo'
  });

  var bp6 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: 'distro-wrapper',
    children: {
      arg: 0
    }
  });

  var bp5 = _inferno2.default.createBlueprint({
    tag: 'div',
    children: {
      arg: 0
    }
  });

  var CommandPanel = exports.CommandPanel = function (_Component) {
    _inherits(CommandPanel, _Component);

    function CommandPanel(props) {
      _classCallCheck(this, CommandPanel);

      var _this = _possibleConstructorReturn(this, (CommandPanel.__proto__ || Object.getPrototypeOf(CommandPanel)).call(this, props));

      _this.state = {
        "expandedCommands": false
      };
      return _this;
    }

    _createClass(CommandPanel, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        // Set background wallpaper
        var wallpaperImage = this.props.backgrounds[Math.floor(Math.random() * (this.props.backgrounds.length - 1))];
        var wallpaperForeground = document.querySelectorAll('.wallpaper-foreground')[0];

        wallpaperForeground.style.background = 'url(\'src/img/backgrounds/' + wallpaperImage + '\')';
        wallpaperForeground.style.backgroundSize = "cover";
      }
    }, {
      key: 'handleCommand',
      value: function handleCommand(command, disabled, event) {
        if (disabled !== false) {
          window.notifications.generate(command + ' is disabled on this system.', "error");
          return false;
        }

        if (command === "power off") {
          window.notifications.generate("Powering off.");
          (0, _utils.endGreeting)(function () {
            return window.lightdm.shutdown();
          });
        } else if (command === "hibernate") {
          window.notifications.generate("Hibernating.");
          (0, _utils.endGreeting)(function () {
            return window.lightdm.hibernate();
          });
        } else if (command === "restart") {
          window.notifications.generate("Restarting.");
          (0, _utils.endGreeting)(function () {
            return window.lightdm.restart();
          });
        } else if (command === "sleep") {
          window.notifications.generate("Suspending.");
          (0, _utils.endGreeting)(function () {
            return window.lightdm.suspend();
          });
        }
      }
    }, {
      key: 'generateCommands',
      value: function generateCommands() {
        var _this2 = this;

        var commands = { // hibernate and suspend were here!
          "Power Off": window.lightdm.can_shutdown,
          "Restart": window.lightdm.can_restart
        };

        // Filter out commands we can't execute.
        var enabledCommands = Object.keys(commands).map(function (key) {
          return commands[key] ? key : false;
        }).filter(function (command) {
          return command !== false;
        });

        var rows = enabledCommands.map(function (command) {
          var disabled = command.toLowerCase().split('.')[1] || false;
          command = command.toLowerCase().split('.')[0];

          var classes = ['command', command, disabled].filter(function (e) {
            return e;
          });

          return bp0(classes.join(' '), {
            onclick: _this2.handleCommand.bind(_this2, command, disabled)
          }, [bp1(bp2()), bp3(command)]);
        });

        var classes = ['commands-wrapper'];

        return bp4(classes.join(' '), rows);
      }
    }, {
      key: 'render',
      value: function render() {
        var hostname = window.lightdm.hostname;
        var commands = this.generateCommands();

        return bp5([bp6(bp7()), commands, bp8([bp9(hostname), bp10(_Clock.Clock)])]);
      }
    }]);

    return CommandPanel;
  }(_infernoComponent2.default);
});