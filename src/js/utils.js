define(['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.endGreeting = endGreeting;
  exports.rander = rander;
  function endGreeting(cb) {
    document.querySelector('html').className += ' ended';
    setTimeout(cb, 1000);
  }

  function rander(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }
});