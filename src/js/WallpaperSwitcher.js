define(['exports', 'src/dist/js/inferno.min', 'src/dist/js/inferno-component.min'], function (exports, _inferno, _infernoComponent) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.WallpaperSwitcher = undefined;

  var _inferno2 = _interopRequireDefault(_inferno);

  var _infernoComponent2 = _interopRequireDefault(_infernoComponent);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  var bp1 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: 'distro-logo'
  });

  var bp0 = _inferno2.default.createBlueprint({
    tag: 'div',
    className: 'distro-wrapper',
    children: {
      arg: 0
    }
  });

  var WallpaperSwitcher = exports.WallpaperSwitcher = function (_Component) {
    _inherits(WallpaperSwitcher, _Component);

    function WallpaperSwitcher(props) {
      _classCallCheck(this, WallpaperSwitcher);

      var _this = _possibleConstructorReturn(this, (WallpaperSwitcher.__proto__ || Object.getPrototypeOf(WallpaperSwitcher)).call(this, props));

      _this.state = {
        "establishedWallpaper": undefined,
        "wallpaperBackground": undefined,
        "wallpaperForeground": undefined
      };
      return _this;
    }

    _createClass(WallpaperSwitcher, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        // Set background wallpaper
        var wallpaperImage = this.props.backgrounds[Math.floor(Math.random() * (this.props.backgrounds.length - 1))];
        var wallpaperBackground = document.querySelectorAll('.wallpaper-background')[0];
        var wallpaperForeground = document.querySelectorAll('.wallpaper-foreground')[0];

        wallpaperForeground.style.background = 'url(\'src/img/backgrounds/' + wallpaperImage + '\')';
        wallpaperForeground.style.backgroundSize = "cover";

        this.setState({
          "establishedWallpaper": wallpaperImage,
          "wallpaperBackground": wallpaperBackground,
          "wallpaperForeground": wallpaperForeground
        });
      }
    }, {
      key: 'render',
      value: function render() {
        return bp0(bp1());
      }
    }]);

    return WallpaperSwitcher;
  }(_infernoComponent2.default);
});